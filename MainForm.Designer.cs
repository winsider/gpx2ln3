﻿namespace GPX2LN3
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dropFiles = new System.Windows.Forms.Label();
            this.files = new System.Windows.Forms.ListBox();
            this.clear = new System.Windows.Forms.Button();
            this.convert = new System.Windows.Forms.Button();
            this.browse = new System.Windows.Forms.Button();
            this.selectFiles = new System.Windows.Forms.OpenFileDialog();
            this.outputFolder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.browseFolder = new System.Windows.Forms.Button();
            this.overwrite = new System.Windows.Forms.CheckBox();
            this.selectFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // dropFiles
            // 
            this.dropFiles.AllowDrop = true;
            this.dropFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dropFiles.BackColor = System.Drawing.SystemColors.Window;
            this.dropFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dropFiles.Location = new System.Drawing.Point(12, 9);
            this.dropFiles.Name = "dropFiles";
            this.dropFiles.Size = new System.Drawing.Size(780, 522);
            this.dropFiles.TabIndex = 0;
            this.dropFiles.Text = "drop files here or click to browse";
            this.dropFiles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.dropFiles.Click += new System.EventHandler(this.browse_Click);
            this.dropFiles.DragDrop += new System.Windows.Forms.DragEventHandler(this.dropFiles_DragDrop);
            this.dropFiles.DragEnter += new System.Windows.Forms.DragEventHandler(this.dropFiles_DragStatus);
            this.dropFiles.DragOver += new System.Windows.Forms.DragEventHandler(this.dropFiles_DragStatus);
            // 
            // files
            // 
            this.files.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.files.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.files.FormattingEnabled = true;
            this.files.ItemHeight = 20;
            this.files.Location = new System.Drawing.Point(12, 9);
            this.files.Name = "files";
            this.files.Size = new System.Drawing.Size(780, 522);
            this.files.TabIndex = 1;
            this.files.Visible = false;
            // 
            // clear
            // 
            this.clear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clear.Location = new System.Drawing.Point(12, 643);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(256, 57);
            this.clear.TabIndex = 2;
            this.clear.Text = "C&lear";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // convert
            // 
            this.convert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.convert.Location = new System.Drawing.Point(536, 644);
            this.convert.Name = "convert";
            this.convert.Size = new System.Drawing.Size(256, 57);
            this.convert.TabIndex = 3;
            this.convert.Text = "&Convert!";
            this.convert.UseVisualStyleBackColor = true;
            this.convert.Click += new System.EventHandler(this.convert_Click);
            // 
            // browse
            // 
            this.browse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.browse.Location = new System.Drawing.Point(274, 643);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(256, 58);
            this.browse.TabIndex = 4;
            this.browse.Text = "&Browse";
            this.browse.UseVisualStyleBackColor = true;
            this.browse.Click += new System.EventHandler(this.browse_Click);
            // 
            // selectFiles
            // 
            this.selectFiles.DefaultExt = "gpx";
            this.selectFiles.FileName = "*.gpx";
            this.selectFiles.Filter = "GPX files|*.gpx";
            this.selectFiles.Multiselect = true;
            this.selectFiles.SupportMultiDottedExtensions = true;
            this.selectFiles.Title = "Select *.gpx files to convert";
            // 
            // outputFolder
            // 
            this.outputFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputFolder.Location = new System.Drawing.Point(12, 612);
            this.outputFolder.Name = "outputFolder";
            this.outputFolder.Size = new System.Drawing.Size(672, 26);
            this.outputFolder.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 588);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 20);
            this.label1.TabIndex = 6;
            this.label1.Text = "Output folder:";
            // 
            // browseFolder
            // 
            this.browseFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.browseFolder.Location = new System.Drawing.Point(690, 607);
            this.browseFolder.Name = "browseFolder";
            this.browseFolder.Size = new System.Drawing.Size(102, 31);
            this.browseFolder.TabIndex = 7;
            this.browseFolder.Text = "Select";
            this.browseFolder.UseVisualStyleBackColor = true;
            this.browseFolder.Click += new System.EventHandler(this.browseFolder_Click);
            // 
            // overwrite
            // 
            this.overwrite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.overwrite.AutoSize = true;
            this.overwrite.Location = new System.Drawing.Point(16, 552);
            this.overwrite.Name = "overwrite";
            this.overwrite.Size = new System.Drawing.Size(187, 24);
            this.overwrite.TabIndex = 8;
            this.overwrite.Text = "overwrite existing files";
            this.overwrite.UseVisualStyleBackColor = true;
            // 
            // selectFolder
            // 
            this.selectFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // MainForm
            // 
            this.AcceptButton = this.convert;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 711);
            this.Controls.Add(this.overwrite);
            this.Controls.Add(this.browseFolder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.outputFolder);
            this.Controls.Add(this.browse);
            this.Controls.Add(this.convert);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.dropFiles);
            this.Controls.Add(this.files);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(826, 564);
            this.Name = "MainForm";
            this.Text = "GPX2LN3 (alpha version 1.0.0.4 july 8 2022)";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label dropFiles;
        private System.Windows.Forms.ListBox files;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button convert;
        private System.Windows.Forms.Button browse;
        private System.Windows.Forms.OpenFileDialog selectFiles;
        private System.Windows.Forms.TextBox outputFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button browseFolder;
        private System.Windows.Forms.CheckBox overwrite;
        private System.Windows.Forms.FolderBrowserDialog selectFolder;
    }
}

