﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GPX2LN3
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            outputFolder.Text = Properties.Settings.Default.OutputFolder;
            overwrite.Checked = Properties.Settings.Default.OverwriteFiles;
            EnableGUI();
        }

        private void dropFiles_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var droppedFiles = (String[])e.Data.GetData(DataFormats.FileDrop);
                int problems = 0;
                foreach (var file in droppedFiles)
                {
                    if (ConvertTools.IsGPX(file))
                    {
                        files.Items.Add(file);
                    }
                    else
                    {
                        problems++;
                    }
                }

                if (problems > 0)
                {
                    MessageBox.Show(this, "One or more files was not the correct type. Files must be .gpx type.", "Problem", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                EnableGUI();
            }
        }

        private void dropFiles_DragStatus(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void clear_Click(object sender, EventArgs e)
        {
            files.Items.Clear();
            EnableGUI();
        }

        private void browse_Click(object sender, EventArgs e)
        {
            if (selectFiles.ShowDialog() == DialogResult.OK)
            {
                foreach (var file in selectFiles.FileNames)
                {
                    files.Items.Add(file);
                }
                EnableGUI();
            }
        }

        private void EnableGUI()
        {
            files.Visible = files.Items.Count > 0;
            dropFiles.Visible = !files.Visible;
            convert.Enabled = files.Visible;
            clear.Enabled = convert.Enabled;
        }

        private void SaveSettings()
        {
            Properties.Settings.Default.OutputFolder = outputFolder.Text;
            Properties.Settings.Default.OverwriteFiles = overwrite.Checked;
            Properties.Settings.Default.Save();
        }

        private void browseFolder_Click(object sender, EventArgs e)
        {
            if (selectFolder.ShowDialog() == DialogResult.OK)
            {
                outputFolder.Text = selectFolder.SelectedPath;
            }
        }

        private void convert_Click(object sender, EventArgs e)
        {
            if (ConvertTools.IsFolderValid(outputFolder.Text))
            {
                int problems = ConvertTools.ConvertFiles(files.Items.Cast<string>().ToArray(), outputFolder.Text, overwrite.Checked);
                MessageBox.Show(this, files.Items.Count - problems + " files out of " + files.Items.Count + " where converted successfully!");
                files.Items.Clear();
                EnableGUI();
                SaveSettings();
            }
            else
            {
                MessageBox.Show(this, "Output folder is not valid!", "Problem", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
