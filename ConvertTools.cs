﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace GPX2LN3
{
    internal class ConvertTools
    {
        public static bool IsGPX(string file)
        {
            return (".gpx" == Path.GetExtension(file).ToLower());
        }

        public static bool IsFolderValid(string folder)
        {
            if (!String.IsNullOrEmpty(folder))
            {
                try
                {
                    string test = Path.GetFullPath(folder);
                    return Path.IsPathRooted(folder);
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        public static int ConvertFiles(string[] files, string outputFolder, bool overwrite)
        {
            int problems = 0;

            foreach (string file in files)
            {
                if (!ConvertFile(file, outputFolder, overwrite))
                {
                    ++problems;
                }
            }

            return problems;
        }

        private static bool ConvertFile(string file, string outputFolder, bool overwrite)
        {
            string newName = Path.Combine(Path.GetDirectoryName(file), Path.GetFileNameWithoutExtension(file) + ".ln3");
            
            if (!String.IsNullOrEmpty(outputFolder))
            {
                newName = Path.Combine(outputFolder, Path.GetFileName(newName));
            }

            if (File.Exists(newName) && !overwrite)
            {
                return false;
            }

            var doc = Flightplan.ReadGpxFile(file);
            doc.WriteLn3File(newName);

            return true;
        }
    }
}
