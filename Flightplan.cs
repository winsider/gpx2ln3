﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace GPX2LN3
{
    internal class Waypoint
    {
        public string Name { get; set; }
        public string Lon { get; set; }
        public string Lat { get; set; }
    }

    internal class Flightplan
    {
        private Flightplan() 
        { 
            Points = new List<Waypoint>();
        }

        public List<Waypoint> Points { get; private set; }

        public static Flightplan ReadGpxFile(string file)
        {
            var gpxDoc = new Flightplan();
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(file);
            var nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("x", "http://www.topografix.com/GPX/1/1");
            foreach (XmlNode node in xmlDoc.SelectNodes("//x:rtept", nsmgr))
            {
                var pt = new Waypoint();
                pt.Lon = node.Attributes["lon"].InnerText;
                pt.Lat = node.Attributes["lat"].InnerText;
                pt.Name = node.SelectSingleNode("x:name", nsmgr).InnerText;
                gpxDoc.Points.Add(pt);
            }

            return gpxDoc;
        }

        public void WriteLn3File(string file)
        {
            using (var writer = new StreamWriter(file, false, Encoding.GetEncoding("ISO-8859-1")))
            {
                int station = 0;
                writer.WriteLine("#");
                writer.WriteLine("Converted with GPX2LN3 tool");
                writer.WriteLine("#");

                foreach (var wpt in Points)
                {
                    writer.WriteLine();
                    writer.WriteLine("[STATION" + ++station + "]");
                    writer.WriteLine("{");
                    writer.WriteLine("LAT: " + wpt.Lat + ";");
                    writer.WriteLine("LON: " + wpt.Lon + ";");
                    writer.WriteLine("NAME: \"" + wpt.Name + "\";");
                    writer.WriteLine("}");
                }
                writer.WriteLine();
            }
        }
    }
}
